﻿namespace Domain.Anemic
{
    public class Frame
    {
        public int Ball1Score { get; set; }
        public int Ball2Score { get; set; }
        public int Ball3Score { get; set; }
    }
}