﻿namespace Domain.Anemic
{
    using System.Collections.Generic;

    public class Game
    {
        public Game()
        {
            Frames = new List<Frame>();
        }

        public IList<Frame> Frames { get; set; }
    }
}