﻿namespace Domain.Anemic
{
    public class ScoringService
    {
        public int ScoreGame(Game game)
        {
            var prevStrike = false;
            var prevSpare = false;
            var totalScore = 0;
            foreach (var frame in game.Frames)
            {
                if (prevStrike)
                {
                    totalScore += frame.Ball1Score + frame.Ball2Score;
                    prevStrike = false;
                }
                else if (prevSpare)
                {
                    totalScore += frame.Ball1Score;
                    prevSpare = false;
                }

                totalScore += frame.Ball1Score + frame.Ball2Score + frame.Ball3Score;
                if (frame.Ball1Score == 10)
                {
                    prevStrike = true;
                }
                else if (frame.Ball1Score + frame.Ball2Score == 10)
                {
                    prevSpare = true;
                }
            }
            return totalScore;
        }
    }
}
