﻿namespace Domain.Tests
{
    using Anemic;
    using NUnit.Framework;
    using Should;

    [TestFixture]
    public class ScoringTests
    {
        [Test]
        public void should_score_zero_when_all_gutters()
        {
            // arrange
            var game = new Game();
            for (var i = 0; i < 10; i++)
            {
                var frame = new Frame { Ball1Score = 0, Ball2Score = 0, Ball3Score = 0 };
                game.Frames.Add(frame);
            }
            var scoreService = new ScoringService();

            // act
            var result = scoreService.ScoreGame(game);

            // assert
            result.ShouldEqual(0);
        }

        [Test]
        public void should_score_sixty_when_all_threes()
        {
            // arrange
            var game = new Game();
            for (var i = 0; i < 10; i++)
            {
                var frame = new Frame { Ball1Score = 3, Ball2Score = 3, Ball3Score = 0 };
                game.Frames.Add(frame);
            }
            var scoreService = new ScoringService();

            // act
            var result = scoreService.ScoreGame(game);

            // assert
            result.ShouldEqual(60);
        }

        [Test]
        public void should_score_twenty_when_spare_followed_by_five_followed_by_gutters()
        {
            // arrange
            var game = new Game();
            var frame1 = new Frame { Ball1Score = 5, Ball2Score = 5, Ball3Score = 0 };
            var frame2 = new Frame { Ball1Score = 5, Ball2Score = 0, Ball3Score = 0 };
            game.Frames.Add(frame1);
            game.Frames.Add(frame2);
            for (var i = 2; i < 10; i++)
            {
                var frame = new Frame { Ball1Score = 0, Ball2Score = 0, Ball3Score = 0 };
                game.Frames.Add(frame);
            }
            var scoreService = new ScoringService();

            // act
            var result = scoreService.ScoreGame(game);

            // assert
            result.ShouldEqual(20);
        }

        [Test]
        public void should_score_twenty_six_when_strike_followed_by_five_followed_by_three_followed_by_gutters()
        {
            // arrange
            var game = new Game();
            var frame1 = new Frame { Ball1Score = 10, Ball2Score = 0, Ball3Score = 0 };
            var frame2 = new Frame { Ball1Score = 5, Ball2Score = 3, Ball3Score = 0 };
            game.Frames.Add(frame1);
            game.Frames.Add(frame2);
            for (var i = 2; i < 10; i++)
            {
                var frame = new Frame { Ball1Score = 0, Ball2Score = 0, Ball3Score = 0 };
                game.Frames.Add(frame);
            }
            var scoreService = new ScoringService();

            // act
            var result = scoreService.ScoreGame(game);

            // assert
            result.ShouldEqual(26);
        }
    }
}
